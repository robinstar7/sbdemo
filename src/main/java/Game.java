import demo.entity.Person;

public class Game {
    public static void main(String[] args) {
        String title = "";
        String job = "";
        System.out.println("拼搏100天");
        System.out.println("=====================");
        //初始化数值（随机）
        int initWisdom = (int)(Math.random()*10);
        int initPower = (int)(Math.random()*10);
        int initCharm = (int)(Math.random()*10);
        int initJob = (int)(Math.random()*10);
        double initRich = 1500;
        if (initJob == 1 || initJob == 2 || initJob == 3){
            job = "码农";
        }
        else if (initJob == 4 || initJob == 5 || initJob == 6){
            job = "土木老哥";
        }
        else if (initJob == 7 || initJob == 8 || initJob == 9){
            job = "爱豆";
        }
        else {

        }
        System.out.println("开局能力值:w-"  + initWisdom + "  ；p-" + initPower + "  ；c-" + initCharm + "   ；岗位：" + job);
        //智慧提升
        int wisdomUp = 1;
        //体质提升
        int powerUp = 1;
        //魅力提升
        int charmUp = 1;
        //富有提升
        double richUp = 1;
        Person person = Person.builder().name("梁俊星").age(25).sex(1)
                .build();
        //拼搏100天
        for (int i = 0; i < 100; i++){
            int number = (int)(Math.random()*10);
            switch (number){
                case 1:
                    System.out.print("第"+ i +"天——");
                    initWisdom = initWisdom + wisdomUp;
                    System.out.println("学习了一天，智慧提升,当前智慧："+ initWisdom);
                    if (job.equals("码农")){
                        double buff = initWisdom / 10;
                        richUp = richUp + buff;
                    }
                    break;
                case 2:
                    System.out.print("第"+ i +"天——");
                    initPower = initPower + powerUp;
                    System.out.println("锻炼了一天，体质提升,当前体质："+ initPower);
                    if (job.equals("土木老哥")){
                        double buff = initPower / 10;
                        richUp = richUp + buff;
                    }
                    break;
                case 3:
                    System.out.print("第"+ i +"天——");
                    initCharm = initCharm + charmUp;
                    System.out.println("唱跳rap一天，魅力提升，当前魅力："+ initCharm);
                    if (job.equals("爱豆")){
                        double buff = initCharm / 10;
                        richUp = richUp + buff;
                    }
                    break;
                case 4:
                    System.out.print("第"+ i +"天——");
                    initRich = initRich + 230 * richUp;
                    System.out.println("努力赚钱的一天，工资提升，当前财富："+ initRich);
                    break;
                case 5:
                    System.out.print("第"+ i +"天——");
                    if (wisdomUp==1){
                        wisdomUp++;
                        System.out.println("认识了一位智者，打开任督二脉，此后智慧提升翻倍，当前："+ wisdomUp);
                    }else {
                        wisdomUp=wisdomUp+2;
                        System.out.println("认识了一位智者，打开任督二脉，此后智慧提升翻倍，当前："+ wisdomUp);
                    }
                    break;
                case 6:
                    System.out.print("第"+ i +"天——");
                    if (powerUp==1){
                        powerUp++;
                        System.out.println("报名了健身班，走上健身道路，此后体质提升翻倍，当前："+ powerUp);
                    }else {
                        powerUp=powerUp+2;
                        System.out.println("报名了健身班，走上健身道路，此后体质提升翻倍，当前："+ powerUp);
                    }
                    break;
                case 7:
                    System.out.print("第"+ i +"天——");
                    if (charmUp==1){
                        charmUp++;
                        System.out.println("走进演艺圈，此后魅力提升翻倍，当前："+ charmUp);
                    }else {
                        charmUp=charmUp+2;
                        System.out.println("走进演艺圈，此后魅力提升翻倍，当前："+ charmUp);
                    }
                    break;
                case 8:
                    System.out.print("第"+ i +"天——");
                    richUp = richUp + 0.5;
                    System.out.println("老板加薪，此后金币提升翻倍，当前："+ richUp);
                    break;
                case 9:
                    System.out.print("第"+ i +"天——");
                    System.out.println("突然生病了，各项能力2天内不提升，财富-300");
                    i = i + 2;
                    initRich = initRich - 300;
                    break;
                default:
                    System.out.println("碌碌无为的一天");
            }
            initRich = initRich -70;
            if (initRich <= 0){
                System.out.println("第"+ i +"天饿死了");
                i = 100;
                title = "《被社会毒打致死》";
            }
        }
        person.setWisdom(initWisdom);
        person.setPower(initPower);
        person.setCharm(initCharm);
        person.setRich(initRich);
        if (initWisdom >= 100){
            title = "《智力高达100，现代爱因斯坦！》";
        }
        if (initPower >= 100){
            title = "《体质高达100，筋肉恶魔人！》";
        }
        if (initCharm >= 100){
            title = "《魅力高达100，宇宙万人迷！》";
        }
        if (initRich >= 10000){
            title = "《财富高达1w，下一个马云！》";
        }
        if (initWisdom < 100 && initPower < 100 && initCharm < 100 && initRich < 10000 && initRich >= 0){
            title = "《平凡的100天》";
        }
        System.out.println(title);
        System.out.println(person);
    }
}
