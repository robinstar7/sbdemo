package demo.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Person {
    String name;
    int age;
    int sex;
    int wisdom;
    int power;
    int charm;
    double rich;
}
